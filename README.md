# Computer graphics

## Name
Application for Cars Dataset analyze

## Description
The program allows you to save, load and modify a database containing information about a limited number of cars (namely 22,702). It is also possible to analyze using graphical and text reports, create pivot tables.
## Visuals
![](https://git.miem.hse.ru/dvshilenko/cars-data-analyze/-/blob/master/img.png)
![](https://git.miem.hse.ru/dvshilenko/cars-data-analyze/-/blob/master/img_1.png)

## Installation
You have to install all necessary files including libraries from requirements.txt

## Support
You can ask for help here: dvshilenko@edu.hse.ru

## Authors and acknowledgment
Project is made with basic knowledge of Python for DS.

## Project status
Project is already completed and doesn't require any changes.
