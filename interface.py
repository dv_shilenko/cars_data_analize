from script_cars import *
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QVBoxLayout, QWidget, QListWidget
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        df = pd.read_excel('cars_dataset_correct.xlsx', nrows=100)
        df_full = pd.read_excel('cars_dataset_correct.xlsx', nrows=1000)
        model = QStandardItemModel(df.shape[0], df.shape[1])
        model.setHorizontalHeaderLabels(df.columns)
        for row in range(df.shape[0]):
            for column in range(df.shape[1]):
                item = QStandardItem(str(df.iloc[row, column]))
                model.setItem(row, column, item)
        numeric = ['Seats', 'Doors', 'Volume', 'Mileage', 'Price', 'Year', 'Engine_volume',
                   'Width', 'Diameter_cylinder_and_stroke_piston', 'Maximum_power', 'Length', 'Wheelbase',
                   'Cylinders', 'Height', 'Torque', 'Volume_fuel_tank', 'Rear_track_width',
                   'Front_track_width', 'Curb_weight_kg', 'Clearance', 'Maximum_speed', 'Speed_to_100']
        non_numeric = []
        for i in df.columns:
            if i not in numeric:
                non_numeric.append(i)
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.df = df
        self.df_full = df_full
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        layout = QVBoxLayout(self.centralwidget)
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        layout.addWidget(self.tabWidget)
        self.tabWidget.setGeometry(QtCore.QRect(0, 0, 800, 600))
        self.tabWidget.setObjectName("tabWidget")
        self.tab_df = QWidget()
        self.tab_df.setObjectName("tab_df")
        self.layout_df = QVBoxLayout(self.tab_df)
        self.dataframe_table = QtWidgets.QTableView(self.tab_df)
        self.dataframe_table.setGeometry(QtCore.QRect(0, 0, 800, 600))
        self.dataframe_table.setObjectName("dataframe_table")
        self.dataframe_table.setModel(model)
        self.layout_df.addWidget(self.dataframe_table)
        self.tabWidget.addTab(self.tab_df, "")
        self.tab_boxplot_graphic = QWidget()
        self.tab_boxplot_graphic.setObjectName("tab_boxplot_graphic")
        self.btn_boxplot_constructor = QtWidgets.QPushButton(self.tab_boxplot_graphic)
        self.btn_boxplot_constructor.setGeometry(QtCore.QRect(660, 30, 91, 40))
        self.btn_boxplot_constructor.setStyleSheet("background-color: rgb(255, 0, 0);\n"
                                                   "color: rgb(255, 255, 255);\n"
                                                   "")
        self.btn_boxplot_constructor.setObjectName("btn_boxplot_constructor")
        self.btn_boxplot_constructor.clicked.connect(self.boxplot_constructor)
        self.boxplot_key1 = QtWidgets.QComboBox(self.tab_boxplot_graphic)
        self.boxplot_key1.setGeometry(QtCore.QRect(10, 30, 200, 40))
        self.boxplot_key1.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.boxplot_key1.setObjectName("boxplot_key1")
        self.boxplot_key2 = QtWidgets.QComboBox(self.tab_boxplot_graphic)
        self.boxplot_key2.setGeometry(QtCore.QRect(230, 30, 200, 40))
        self.boxplot_key2.setObjectName("boxplot_key2")
        self.boxplot_key1.addItem('Выберите колонку')
        self.boxplot_key2.addItem('Выберите колонку')
        for i in numeric:
            self.boxplot_key2.addItem(i)
        for i in non_numeric:
            self.boxplot_key1.addItem(i)
        self.tabWidget.addTab(self.tab_boxplot_graphic, "")
        self.tab_scatter_graphic = QWidget()
        self.tab_scatter_graphic.setObjectName("tab_scatter_graphic")
        self.btn_scatter_constructor = QtWidgets.QPushButton(self.tab_scatter_graphic)
        self.btn_scatter_constructor.setGeometry(QtCore.QRect(660, 30, 91, 40))
        self.btn_scatter_constructor.setStyleSheet("background-color: rgb(255, 0, 0);\n"
                                                   "color: rgb(255, 255, 255);\n"
                                                   "")
        self.btn_scatter_constructor.setObjectName("btn_scatter_constructor")
        self.btn_scatter_constructor.clicked.connect(self.scatter_constructor)
        self.scatter_key1 = QtWidgets.QComboBox(self.tab_scatter_graphic)
        self.scatter_key1.setGeometry(QtCore.QRect(10, 30, 200, 40))
        self.scatter_key1.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.scatter_key1.setObjectName("scatter_key1")
        self.scatter_key2 = QtWidgets.QComboBox(self.tab_scatter_graphic)
        self.scatter_key2.setGeometry(QtCore.QRect(230, 30, 200, 40))
        self.scatter_key2.setObjectName("scatter_key2")
        self.scatter_key1.addItem('Выберите колонку')
        self.scatter_key2.addItem('Выберите колонку')
        for i in numeric:
            self.scatter_key2.addItem(i)
            self.scatter_key1.addItem(i)
        self.tabWidget.addTab(self.tab_scatter_graphic, "")
        self.tab_bar = QtWidgets.QWidget()
        self.tab_bar.setObjectName("tab_bar")
        self.bar_key1 = QtWidgets.QComboBox(self.tab_bar)
        self.bar_key1.setGeometry(QtCore.QRect(20, 30, 180, 40))
        self.bar_key1.setObjectName("bar_key1")
        self.bar_key1.addItem('Выберите колонку')
        for i in non_numeric:
            self.bar_key1.addItem(i)
        self.bar_key2 = QtWidgets.QComboBox(self.tab_bar)
        self.bar_key2.setGeometry(QtCore.QRect(210, 30, 180, 40))
        self.bar_key2.setObjectName("bar_key2")
        self.bar_key2.addItem('Выберите колонку')
        for i in numeric:
            self.bar_key2.addItem(i)
        self.label_11 = QtWidgets.QLabel(self.tab_bar)
        self.label_11.setGeometry(QtCore.QRect(20, 500, 600, 31))
        self.label_11.setObjectName("label_11")
        self.btn_bar_constructor = QtWidgets.QPushButton(self.tab_bar)
        self.btn_bar_constructor.setGeometry(QtCore.QRect(660, 30, 91, 40))
        self.btn_bar_constructor.setStyleSheet("background-color: rgb(255, 0, 0);\n"
                                               "color: rgb(255, 255, 255);\n"
                                               "")
        self.btn_bar_constructor.setObjectName("btn_bar_constructor")
        self.btn_bar_constructor.clicked.connect(self.bar_constructor)
        self.tabWidget.addTab(self.tab_bar, "")

        self.tab_freq_bar = QtWidgets.QWidget()
        self.tab_freq_bar.setObjectName("tab_freq_bar")
        self.freq_bar_key1 = QtWidgets.QComboBox(self.tab_freq_bar)
        self.freq_bar_key1.setGeometry(QtCore.QRect(20, 30, 180, 40))
        self.freq_bar_key1.setObjectName("bar_key1")
        self.freq_bar_key1.addItem('Выберите колонку')
        for i in non_numeric:
            self.freq_bar_key1.addItem(i)
        self.btn_freq_bar_constructor = QtWidgets.QPushButton(self.tab_freq_bar)
        self.btn_freq_bar_constructor.setGeometry(QtCore.QRect(660, 30, 91, 40))
        self.btn_freq_bar_constructor.setStyleSheet("background-color: rgb(255, 0, 0);\n"
                                               "color: rgb(255, 255, 255);\n"
                                               "")
        self.btn_freq_bar_constructor.setObjectName("btn_freq_bar_constructor")
        self.btn_freq_bar_constructor.clicked.connect(self.freq_bar_constructor)
        self.tabWidget.addTab(self.tab_freq_bar, "")

        self.tab_hist = QtWidgets.QWidget()
        self.tab_hist.setObjectName("tab_hist")
        self.hist_key1 = QtWidgets.QComboBox(self.tab_hist)
        self.hist_key1.setGeometry(QtCore.QRect(30, 20, 200, 40))
        self.hist_key1.setObjectName("hist_key1")
        self.hist_key1.addItem('Выберите колонку')
        for i in non_numeric:
            self.hist_key1.addItem(i)
        self.label_2 = QtWidgets.QLabel(self.tab_hist)
        self.label_2.setGeometry(QtCore.QRect(40, 95, 600, 16))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.tab_hist)
        self.label_3.setGeometry(QtCore.QRect(40, 120, 600, 16))
        self.label_3.setObjectName("label_3")
        self.btn_hist_constructor = QtWidgets.QPushButton(self.tab_hist)
        self.btn_hist_constructor.setGeometry(QtCore.QRect(660, 20, 91, 40))
        self.btn_hist_constructor.setStyleSheet("background-color: rgb(255, 0, 0);\n"
                                                "color: rgb(255, 255, 255);\n"
                                                "")
        self.btn_hist_constructor.setObjectName("btn_hist_constructor")
        self.btn_hist_constructor.clicked.connect(self.hist_constructor)
        self.hist_key2 = QtWidgets.QComboBox(self.tab_hist)
        self.hist_key2.setGeometry(QtCore.QRect(30, 170, 200, 40))
        self.hist_key2.setObjectName("hist_key2")
        self.hist_key2.addItem('Выберите колонку')
        self.hist_key2.addItem('Country')
        self.hist_key2.addItem('Fuel')
        self.hist_key2.addItem('Year')
        self.hist_key2.addItem('Color')
        self.hist_key2.addItem('Car_type')
        self.hist_key2.currentIndexChanged.connect(self.hist_option_changed)
        self.hist_option = QtWidgets.QComboBox(self.tab_hist)
        self.hist_option.setGeometry(QtCore.QRect(240, 170, 200, 40))
        self.hist_key_name = QtWidgets.QComboBox(self.tab_hist)
        self.hist_key_name.setGeometry(QtCore.QRect(30, 230, 200, 40))
        self.hist_key_name.addItem('Выберите тип графика')
        self.hist_key_name.addItem('Fridman-Diaconis')
        self.hist_key_name.addItem('Sterges')
        self.hist_key_name.addItem('Qvantile')
        self.hist_key_name.addItem('Scott')
        self.hist_option.setObjectName("hist_option")
        self.tabWidget.addTab(self.tab_hist, "")
        self.tab_text_report1 = QtWidgets.QWidget()
        self.tab_text_report1.setObjectName("tab_text_report1")
        self.report_table = QListWidget(self.tab_text_report1)
        self.report_table.setSelectionMode(QListWidget.MultiSelection)
        self.report_table.setGeometry(0, 40, 800, 400)
        self.report_table.addItems(df.columns)
        self.btn_report1 = QtWidgets.QPushButton(self.tab_text_report1)
        self.btn_report1.setGeometry(QtCore.QRect(660, 480, 91, 40))
        self.btn_report1.setStyleSheet("background-color: rgb(255, 0, 0);\n"
                                       "color: rgb(255, 255, 255);")
        self.btn_report1.setObjectName("btn_report1")
        self.label_6 = QtWidgets.QLabel(self.tab_text_report1)
        self.label_6.setGeometry(QtCore.QRect(10, 0, 400, 40))
        self.label_6.setObjectName("label_6")
        self.label_5 = QtWidgets.QLabel(self.tab_text_report1)
        self.label_5.setGeometry(QtCore.QRect(10, 440, 400, 40))
        self.label_5.setObjectName("label_5")
        self.report_start = QtWidgets.QLineEdit(self.tab_text_report1)
        self.report_start.setGeometry(QtCore.QRect(10, 480, 90, 40))
        self.label_4 = QtWidgets.QLabel(self.tab_text_report1)
        self.label_4.setGeometry(QtCore.QRect(120, 440, 400, 40))
        self.label_4.setObjectName("label_4")
        self.report_end = QtWidgets.QLineEdit(self.tab_text_report1)
        self.report_end.setGeometry(QtCore.QRect(120, 480, 90, 40))
        self.btn_report1.clicked.connect(self.slice_df)
        self.tabWidget.addTab(self.tab_text_report1, "")

        self.tab_text_report23 = QWidget()
        self.label_7 = QtWidgets.QLabel(self.tab_text_report23)
        self.label_7.setGeometry(QtCore.QRect(10, 10, 400, 40))
        self.label_7.setObjectName("label_7")
        self.report2_key = QtWidgets.QComboBox(self.tab_text_report23)
        self.report2_key.setGeometry(QtCore.QRect(10, 80, 180, 40))
        self.report2_key.setObjectName("report2_key")
        self.report2_key.addItem('Выберите столбец')
        self.report2_key.addItems(non_numeric)
        self.btn_report2 = QtWidgets.QPushButton(self.tab_text_report23)
        self.btn_report2.setGeometry(QtCore.QRect(300, 80, 91, 40))
        self.btn_report2.setStyleSheet("background-color: rgb(255, 0, 0);\n"
                                       "color: rgb(255, 255, 255);")
        self.btn_report2.setObjectName("btn_report2")
        self.btn_report2.clicked.connect(self.report_non_numeric)
        self.btn_report3 = QtWidgets.QPushButton(self.tab_text_report23)
        self.btn_report3.setGeometry(QtCore.QRect(300, 250, 91, 40))
        self.btn_report3.setStyleSheet("background-color: rgb(255, 0, 0);\n"
                                       "color: rgb(255, 255, 255);")
        self.btn_report3.setObjectName("btn_report3")
        self.btn_report3.clicked.connect(self.report_numeric)
        self.label_9 = QtWidgets.QLabel(self.tab_text_report23)
        self.label_9.setGeometry(QtCore.QRect(10, 180, 451, 16))
        self.label_9.setObjectName("label_9")
        self.report3_key = QtWidgets.QComboBox(self.tab_text_report23)
        self.report3_key.setGeometry(QtCore.QRect(10, 250, 180, 40))
        self.report3_key.setObjectName("report3_key")
        self.report3_key.addItem('Выберите столбец')
        self.report3_key.addItems(numeric)
        self.tabWidget.addTab(self.tab_text_report23, "")
        self.tab_add_column = QtWidgets.QWidget()
        self.tab_add_column.setObjectName("tab_add_column")
        self.add_column_name = QtWidgets.QLineEdit(self.tab_add_column)
        self.add_column_name.setGeometry(QtCore.QRect(10, 60, 200, 40))
        self.label_10 = QtWidgets.QLabel(self.tab_add_column)
        self.label_10.setGeometry(QtCore.QRect(10, 20, 400, 16))
        self.label_10.setObjectName("label_10")
        self.add_column_array = QtWidgets.QLineEdit(self.tab_add_column)
        self.add_column_array.setGeometry(QtCore.QRect(10, 160, 400, 40))
        self.label_11 = QtWidgets.QLabel(self.tab_add_column)
        self.label_11.setGeometry(QtCore.QRect(10, 120, 400, 16))
        self.label_11.setObjectName("label_11")
        self.btn_add_column = QtWidgets.QPushButton(self.tab_add_column)
        self.btn_add_column.setGeometry(QtCore.QRect(660, 60, 91, 40))
        self.btn_add_column.setStyleSheet("background-color: rgb(255, 0, 0);\n"
                                       "color: rgb(255, 255, 255);")
        self.btn_add_column.setObjectName("btn_add_column")
        self.btn_add_column.clicked.connect(self.add_column)
        self.tabWidget.addTab(self.tab_add_column, "")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 449, 18))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(5)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_df), _translate("MainWindow", "База данных"))
        self.btn_boxplot_constructor.setText(_translate("MainWindow", "Построить"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_boxplot_graphic),
                                  _translate("MainWindow", "Усатые графики"))
        self.btn_scatter_constructor.setText(_translate("MainWindow", "Построить"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_scatter_graphic),
                                  _translate("MainWindow", "Точечные графики"))
        self.label_11.setText(
            _translate("MainWindow", "Учитывайте, что первый столбец - качественный, а второй - количественный"))
        self.btn_bar_constructor.setText(_translate("MainWindow", "Построить"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_bar),
                                  _translate("MainWindow", "Столбчатые диаграммы"))
        self.btn_freq_bar_constructor.setText(_translate("MainWindow", "Построить"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_freq_bar),
                                  _translate("MainWindow", "Точечные диаграммы"))
        self.label_2.setText(_translate("MainWindow", "Если желаете построить график с условием, то"))
        self.label_3.setText(_translate("MainWindow", "выберите столбец и усолвие:"))
        self.btn_hist_constructor.setText(_translate("MainWindow", "Построить"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_hist), _translate("MainWindow", "Гистограммы"))
        self.label_5.setText(_translate("MainWindow", "Номер начала:"))
        self.label_4.setText(_translate("MainWindow", "Номер конца:"))
        self.label_6.setText(_translate("MainWindow", "Выберите столбцы, которые хотите добавить:"))
        self.label_7.setText(_translate("MainWindow", "Отчет для столбцов с качественными значениями:"))
        self.btn_report1.setText(_translate("MainWindow", "Показать"))
        self.btn_report2.setText(_translate("MainWindow", "Показать"))
        self.btn_report3.setText(_translate("MainWindow", "Показать"))
        self.label_9.setText(_translate("MainWindow", "Отчет для столбцов с количественными значениями:"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_text_report1),
                                  _translate("MainWindow", "Срез БД"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_text_report23),
                                  _translate("MainWindow", "Текстовый отчет"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_add_column),
                                  _translate("MainWindow", "Добавление столбца"))
        self.label_10.setText(_translate("MainWindow",
                                         "Введите название столбца:"))
        self.label_11.setText(_translate("MainWindow",
                                         "Введите элементы через запятую:"))
        self.btn_add_column.setText(_translate("MainWindow", "Показать"))

    def boxplot_constructor(self):
        if self.boxplot_key1.currentIndex() != 0 and self.boxplot_key2.currentIndex() != 0:
            self.plot_window = PlotWindow(boxplot_graph(self.df, self.boxplot_key1.currentText(),
                                                        self.boxplot_key2.currentText()))
            self.plot_window.show()
        else:
            error = QtWidgets.QMessageBox()
            error.setWindowTitle('Ошибка')
            error.setText('Выберите обе колонки!')
            error.setIcon(QtWidgets.QMessageBox.Icon.Warning)
            error.setStandardButtons(QtWidgets.QMessageBox.Ok)
            error.exec_()

    def bar_constructor(self):
        if self.bar_key1.currentIndex() != 0 and self.bar_key2.currentIndex() != 0:
            self.plot_window = PlotWindow(bar_graph(self.df, self.bar_key1.currentText(),
                                                    self.bar_key2.currentText()))
            self.plot_window.show()
        else:
            error = QtWidgets.QMessageBox()
            error.setWindowTitle('Ошибка')
            error.setText('Выберите обе колонки!')
            error.setIcon(QtWidgets.QMessageBox.Icon.Warning)
            error.setStandardButtons(QtWidgets.QMessageBox.Ok)
            error.exec_()

    def scatter_constructor(self):
        if self.scatter_key1.currentIndex() != 0 and self.scatter_key2.currentIndex() != 0:
            self.plot_window = PlotWindow(scatter_graph(self.df, self.scatter_key1.currentText(),
                                                        self.scatter_key2.currentText()))
            self.plot_window.show()
        else:
            error = QtWidgets.QMessageBox()
            error.setWindowTitle('Ошибка')
            error.setText('Выберите обе колонки!')
            error.setIcon(QtWidgets.QMessageBox.Icon.Warning)
            error.setStandardButtons(QtWidgets.QMessageBox.Ok)
            error.exec_()

    def hist_option_changed(self):
        self.hist_option.clear()
        self.hist_option.addItem('Выберите параметр')
        if self.hist_key2.currentIndex() == 1:
            for i in self.df['Country'].unique():
                self.hist_option.addItem(i)
        elif self.hist_key2.currentIndex() == 2:
            for i in self.df['Fuel'].unique():
                self.hist_option.addItem(i)
        elif self.hist_key2.currentIndex() == 3:
            for i in self.df['Year'].unique():
                self.hist_option.addItem(str(i))
        elif self.hist_key2.currentIndex() == 4:
            for i in self.df['Color'].unique():
                self.hist_option.addItem(i)
        else:
            for i in self.df['Car_type'].unique():
                self.hist_option.addItem(i)

    def hist_constructor(self):
        if self.hist_key1.currentIndex() != 0 and self.hist_key2.currentIndex() == 0 \
                and self.hist_key_name.currentIndex() != 0:
            self.plot_window = PlotWindow(hist_graph(self.df_full, self.hist_key1.currentText(),
                                                     self.hist_key_name.currentText()))
            self.plot_window.show()
        elif self.hist_key1.currentIndex() != 0 and self.hist_key2.currentIndex() != 0 \
                and self.hist_option.currentIndex() != 0 and self.hist_key_name.currentIndex() != 0:
            self.plot_window = PlotWindow(hist_graph(self.df_full, self.hist_key1.currentText(),
                                                     self.hist_key_name.currentText(), self.hist_key2.currentText(),
                                                     self.hist_option.currentText()))
            self.plot_window.show()
        else:
            error = QtWidgets.QMessageBox()
            error.setWindowTitle('Ошибка')
            error.setText('Выберите все параметры!')
            error.setIcon(QtWidgets.QMessageBox.Icon.Warning)
            error.setStandardButtons(QtWidgets.QMessageBox.Ok)
            error.exec_()

    def slice_df(self):
        if len(self.report_table.selectedItems()) > 0 and self.report_start.text() != '' \
                and self.report_start.text().isdigit() and self.report_end.text() != '' \
                and self.report_end.text().isdigit():
            if int(self.report_start.text()) < int(self.report_end.text()):
                selected_texts = [item.text() for item in self.report_table.selectedItems()]
                new_data = cut_data(self.df_full, int(self.report_start.text()),
                                    int(self.report_end.text()), selected_texts)
                self.table_window = TableWindow(new_data)
                self.table_window.show()
            else:
                error = QtWidgets.QMessageBox()
                error.setWindowTitle('Ошибка')
                error.setText('Индекс конца не меньше индекса начала')
                error.setIcon(QtWidgets.QMessageBox.Icon.Warning)
                error.setStandardButtons(QtWidgets.QMessageBox.Ok)
                error.exec_()
        else:
            error = QtWidgets.QMessageBox()
            error.setWindowTitle('Ошибка')
            error.setText('Выберите все параметры!')
            error.setIcon(QtWidgets.QMessageBox.Icon.Warning)
            error.setStandardButtons(QtWidgets.QMessageBox.Ok)
            error.exec_()

    def report_non_numeric(self):
        if self.report2_key.currentIndex() != 0:
            new_data = specifications_report(self.df_full, self.report2_key.currentText())
            self.table_window = TableWindow(new_data)
            self.table_window.show()
        else:
            error = QtWidgets.QMessageBox()
            error.setWindowTitle('Ошибка')
            error.setText('Выберите колонку!')
            error.setIcon(QtWidgets.QMessageBox.Icon.Warning)
            error.setStandardButtons(QtWidgets.QMessageBox.Ok)
            error.exec_()

    def report_numeric(self):
        if self.report3_key.currentIndex() != 0:
            new_data = numeral_report(self.df_full, self.report3_key.currentText())
            self.table_window = TableWindow(new_data)
            self.table_window.show()
        else:
            error = QtWidgets.QMessageBox()
            error.setWindowTitle('Ошибка')
            error.setText('Выберите колонку!')
            error.setIcon(QtWidgets.QMessageBox.Icon.Warning)
            error.setStandardButtons(QtWidgets.QMessageBox.Ok)
            error.exec_()

    def add_column(self):
        if self.add_column_name.text() != '' and self.add_column_array.text() != '':
            array = self.add_column_array.text().split(',')
            new_data = add_column(self.df,
                                  self.add_column_name.text(),
                                  array)
            self.table_window = TableWindow(new_data)
            self.table_window.show()
        else:
            error = QtWidgets.QMessageBox()
            error.setWindowTitle('Ошибка')
            error.setText('Введите все данные!')
            error.setIcon(QtWidgets.QMessageBox.Icon.Warning)
            error.setStandardButtons(QtWidgets.QMessageBox.Ok)
            error.exec_()

    def freq_bar_constructor(self):
        if self.freq_bar_key1.currentIndex() != 0:
            self.plot_window = PlotWindow(freq_bar(self.df, self.freq_bar_key1.currentText()))
            self.plot_window.show()
        else:
            error = QtWidgets.QMessageBox()
            error.setWindowTitle('Ошибка')
            error.setText('Выберите колонку!')
            error.setIcon(QtWidgets.QMessageBox.Icon.Warning)
            error.setStandardButtons(QtWidgets.QMessageBox.Ok)
            error.exec_()


class PlotWindow(QWidget):
    def __init__(self, figure):
        super().__init__()
        self.setWindowTitle("Новое окно")

        self.figure = figure
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)

        layout = QVBoxLayout()
        layout.addWidget(self.canvas)
        layout.addWidget(self.toolbar)

        self.setLayout(layout)


class TableWindow(QWidget):
    def __init__(self, df, parent=None):
        super().__init__(parent)
        self.df = df
        self.setWindowTitle("База данных")
        self.setGeometry(200, 200, 800, 600)

        layout = QVBoxLayout(self)

        # Create a QTableView widget
        self.table_view = QtWidgets.QTableView()
        layout.addWidget(self.table_view)
        button_box = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok)
        layout.addWidget(button_box)
        button_box.button(QtWidgets.QDialogButtonBox.Ok).setText("Скачать")
        button_box.accepted.connect(self.download_table)

        model = QStandardItemModel(df.shape[0], df.shape[1])
        model.setHorizontalHeaderLabels(df.columns)
        for row in range(df.shape[0]):
            for column in range(df.shape[1]):
                item = QStandardItem(str(df.iloc[row, column]))
                model.setItem(row, column, item)

        self.table_view.setModel(model)

    def download_table(self):
        # Get the model from the QTableView
        model = self.table_view.model()

        # Create a DataFrame to store the table data
        df = pd.DataFrame(columns=[model.headerData(col, Qt.Horizontal) for col in range(model.columnCount())])
        for row in range(model.rowCount()):
            data = [model.data(model.index(row, col), Qt.DisplayRole) for col in range(model.columnCount())]
            df.loc[row] = data

        # Open a file dialog to choose the file format and save path
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        file_name, _ = QtWidgets.QFileDialog.getSaveFileName(self,
                                                             "Save Table",
                                                             "",
                                                             "CSV Files (*.csv);;Excel Files (*.xlsx)",
                                                             options=options)

        if file_name:
            # Determine the file format based on the file extension
            file_extension = file_name.split(".")[-1].lower()
            if file_extension == "csv":
                df.to_csv(file_name, index=False)
                print("Table downloaded as CSV!")
            elif file_extension == "xlsx":
                df.to_excel(file_name, index=False)
                print("Table downloaded as Excel!")


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    ui.tabWidget.setCurrentIndex(0)
    MainWindow.show()
    sys.exit(app.exec_())
