"""
Script with analize and handling finctions
"""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


# bar graphic of 2 columns dependence
def bar_graph(tab, str1: str, str2: str):
    """
    bar graphic for depending columns

    Parameters
    ----------
    data : DataFrame
        full data
    a : str
        name of main column
    b : str
        name of depending column

    Returns
    bar

    """
    fig = plt.figure(figsize=(100, 12))
    x_label = tab[1:500][str1]
    # x = np.unique(x)
    y_label = tab[1:500][str2]
    plt.title(f'Зависимость {str2} от {str1}')
    plt.xlabel(f"{str1}")
    plt.ylabel(f"{str2}")
    plt.bar(x_label, y_label)
    return fig


# bar graphic for frequency visualisation
def freq_bar(tab, key: str):
    """
    bar frequency graphic

    Parameters
    ----------
    data : DataFrame
        full data
    key : str
        name of column

    Returns
    freq_bar

    """
    counts = tab[key].value_counts()
    counts = counts.to_frame()
    counts.rename(columns={key: 'counts'}, inplace=True)
    counts.index.rename(key, inplace=True)
    counts = counts.sort_values(by='counts', ascending=False)
    fig = plt.figure(figsize=(100, 12))
    plt.bar(counts.index, counts['counts'])
    return fig


# histogramma that visualizes some specifications
# 4 types of improving accuracy
def hist_graph(tab, key1: str, name: str, key2='\\', val=''):
    """

    Parameters
    ----------
    data : DataFrame
        start data
    key1 : str
        key to research
    key2 : str
        key to define part of data
    val : str
        value to define part of data
    name : str
        method of improving accuracy

    Returns
    -------
    fig

    """
    if key2 == '\\':
        inform = tab[key1]
    else:
        inform = tab[tab[key2] == val][key1]

    if name == 'Fridman-Diaconis':
        # Fridman-Diaconis
        iqr = np.subtract(*np.percentile(inform, [75, 25]))
        h_variable = 2 * iqr * len(inform) ** (-1/3)
        num = int(np.ceil((max(inform) - min(inform)) / h_variable))
        fig = plt.figure(figsize=(12, 8))
        plt.hist(inform, bins=num, edgecolor='black', color='blue')
        plt.xlabel(key1)
        plt.ylabel('Frequency')
        plt.title('Fridman-Diaconis histogram')
        return fig
    if name == 'Sterges':
        # Sterges
        num = np.ceil(1 + np.log2(len(inform))).astype(int)
        fig = plt.figure(figsize=(12, 8))
        plt.hist(inform, bins=num, edgecolor='black', color='red')
        plt.xlabel(key1)
        plt.ylabel('Frequency')
        plt.title('Sterges')
        return fig
    if name == 'Qvantile':
        # Qvantile
        min_val = min(inform)
        max_val = max(inform)
        bin_width = (max_val - min_val) / (len(inform) / 600)
        num = np.ceil((max_val - min_val) / bin_width).astype(int)
        fig = plt.figure(figsize=(12, 8))
        plt.hist(inform, bins=num, edgecolor='black', color='green')
        plt.xlabel(key1)
        plt.ylabel('Frequency')
        plt.title('Qvantile')
        return fig
    if name == 'Scott':
        # Scott
        std_dev = np.std(inform)
        bin_width = 3.5 * std_dev * len(inform) ** (-1/3)
        num = int(np.ceil((max(inform) - min(inform)) / bin_width))
        fig = plt.figure(figsize=(12, 8))
        plt.hist(inform, bins=num, edgecolor='black', color='violet')
        plt.xlabel(key1)
        plt.ylabel('Frequency')
        plt.title('Scott')
        return fig


# scatter visualization
def scatter_graph(tab, key1: str, key2: str):
    """

    Parameters
    ----------
    data : DataFrame
        full data
    key1 : str
        first column name
    key2 : str
        second column name

    Returns
    -------
    None.

    """
    fig = plt.figure(figsize=(12, 8))
    plt.xlabel(key1)
    plt.ylabel(key2)
    plt.title(f"Зависимость {key2} от {key1}")
    plt.scatter(tab[key1], tab[key2])
    return fig


# boxplot vizualization
def boxplot_graph(tab, key1: str, key2: str):
    """

    Parameters
    ----------
    data : DataFrame
        start data
    key1 : str
        key to define unique data
    key2 : str
        key to analyze

    Returns
    -------
    None.

    """
    arr = tab[key1].unique()
    inform = []
    for i in arr:
        inform.append(tab[tab[key1] == i][key2])
    fig = plt.figure(figsize=(12, 8))
    plt.title(f'Boxplot for {key2} with unique {key1}')
    plt.boxplot(inform)
    return fig


# text specifications report
def specifications_report(tab, key: str):
    """

    Parameters
    ----------
    data : DataFrame
        full data
    key : str
        column key

    Returns
    -------
    counts : DataFrame
        table with specifications

    """
    counts = tab[key].value_counts()
    counts = counts.to_frame()
    counts.rename(columns={'Index': key, key: 'counts'}, inplace=True)
    percentage = []
    for i in counts:
        percentage = 100 * counts[i] / counts['counts'].sum()
    counts['percentage'] = percentage
    array = []
    for i in counts.index:
        array.append(i)
    counts.insert(0, 'information', array)
    return counts


# report with numeral data specifications
def numeral_report(tab, key: str):
    """


    Parameters
    ----------
    data : DataFrame
        data for analyze
    key : str
        name of column to analyze

    Returns
    -------
    info : DataFrame
        numeral report

    """
    inform = tab[key]
    # variance
    num = len(inform)
    mean_calc = sum(inform) / num
    variance_calc = sum((item - mean_calc)**2 for item in inform) / (num - 1)
    # mode
    mode_calc = tab[key].mode()

    inform = inform.describe()
    inform = inform.to_frame()
    inform.loc['mode'] = mode_calc[0]
    inform.loc['variance'] = variance_calc
    inform.loc['mean_calc'] = mean_calc
    array = []
    for i in inform.index:
        array.append(i)
    inform.insert(0, 'stat', array)
    return inform


# add column func
def add_column(tab, key: str, array):
    """
    You should slice DataFrame otherwise you must fill 22 000 strings

    Parameters
    ----------
    data : DataFrame
        Frame to fill
    start : int
        start of data-height slice
    end : int
        end of data-height slice
    key : str
        name of new column
    array : TYPE
        array with data for new column

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    data1 = tab.copy()
    index = []
    for i in range(len(array)):
        index.append(i)
    temp = pd.Series(array, index=index)
    data1.insert(0, key, temp, allow_duplicates=True)
    return data1


# data cutting func
def cut_data(tab, start_0: int, end_0: int, array):
    """

    Parameters
    ----------
    data : DataFrame
        data to cut
    start_0 : int
        stert of string slice
    end_0 : int
        end of string slice
    array : array_object
        array with column names to keep

    Returns
    -------
    data : DataFrame
        cut data

    """
    data1 = pd.DataFrame()
    for i in array:
        data1[i] = tab[start_0:end_0 + 1][i]
    return data1


# save data to excel
def save_data(path: str, tab):
    """

    Parameters
    ----------
    path : str
        full path for data saving

    Returns
    -------
    None.

    """
    tab.to_excel(f"{path}.xlsx")
